import numpy as np
import cv2


def _order_points(pts):
    rect = np.zeros((4, 2), dtype = "float32")

    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect


def _get_transform_params(pts):
    rect = _order_points(pts)
    (tl, tr, br, bl) = rect

    widthA = np.sqrt(((br[0]-bl[0])**2) + ((br[1]-bl[1])**2))
    widthB = np.sqrt(((tr[0]-tl[0])**2) + ((tr[1]-tl[1])**2))
    width = max(int(widthA), int(widthB))

    heightA = np.sqrt(((tr[0]-br[0])**2) + ((tr[1]-br[1])**2))
    heightB = np.sqrt(((tl[0]-bl[0])**2) + ((tl[1]-bl[1])**2))
    height = max(int(heightA), int(heightB))

    dst = np.float32([[0, 0],
                      [width-1, 0],
                      [width-1, height-1],
                      [0, height-1]])

    M = cv2.getPerspectiveTransform(rect, dst)

    return {
                'M': M,
                'w': width,
                'h': height
           }


def pts_transform(point_set, pts_4):
    M = _get_transform_params(pts_4)['M']
    points_destination = cv2.perspectiveTransform(point_set, M)
    return points_destination


def img_transform(image, pts_4):
    tp = _get_transform_params(pts_4)
    transformed = cv2.warpPerspective(image, tp['M'],
                                      (tp['w'], tp['h']))

    return transformed


if __name__ == '__main__':
    img = cv2.imread('input.jpg')

    top_left_src = [278, 126]
    bottom_left_src = [62, 484] 
    top_right_src = [526, 251]
    bottom_right_src = [338, 638]

    source_vertices = np.float32([top_left_src, bottom_left_src,
                                  bottom_right_src, top_right_src])

    point_set = np.float32([[[0,0], [1, 1], [2, 2], [3, 3]]])
    print('point_set=', point_set)
    point_set_transformed = pts_transform(point_set, source_vertices)
    print('point_set_transformed=', point_set_transformed)

    transformed_img = img_transform(img, source_vertices)

    cv2.imshow('transformed_img', transformed_img)
    cv2.waitKey(-1)
